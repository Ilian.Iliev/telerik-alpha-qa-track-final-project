package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class DeletePostConfirmationPage extends BasePage {

    public DeletePostConfirmationPage(UserActions actions) {
        super(actions, "weare.deletePostConfirmationPage");
    }

    public void assertPageNavigated() {
        super.assertPageNavigated();
        actions.waitForElementVisible("weare.deletePostPage.confirmDropdown");
    }

    public void confirmDelete(boolean confirm){
        actions.selectValueFromDropdown(confirm ? "Delete post" : "Cancel",
                "weare.deletePostPage.confirmDropdown");
        actions.clickElement("weare.deletePostPage.confirmButton");
    }

    public void assertPostIsDeleted(){
        actions.waitForElementVisible("weare.deletePostPage.confirmationMessage");
    }
}

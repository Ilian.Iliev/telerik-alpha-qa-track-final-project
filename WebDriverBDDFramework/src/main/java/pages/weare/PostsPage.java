package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class PostsPage extends BasePage {

    public PostsPage(UserActions actions) {
        super(actions, "weare.postsPage");
    }

    @Override
    public void assertPageNavigated() {
        super.assertPageNavigated();
        actions.waitForElementVisible("weare.postsPage.head");
    }


    public void explorePostByContent(String content){
        actions.clickElement("weare.postsPage.explorePostButtonByContent",content);
    }

}

package tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConnectionsTests extends BaseTest{

    @BeforeEach
    public void signIn(){
        super.singIn();
    }

    @AfterEach
    public void logOut(){
        super.logOut();
    }

    @Test
    public void connectAndDisconnectWithExistingUser(){
        home.searchForExistingUserByName("Jane Doe");
        home.assertForExistingUserByName("Jane Doe");
        profile.clickSeeProfileButton();
        profile.clickConnectButton();
        home.clickLogOutButton();
        home.clickSignInButton();
        login.enterUsername("UserTwo");
        login.enterPassword("123456");
        login.clickLoginButton();
        home.openProfilePage();
        profile.clickNewFriendsRequestButton();
        requests.clickApproveButton();
        home.navigateToPage();
        home.searchForExistingUserByName("John Doe");

        profile.clickSeeProfileButton();
        profile.assertForOneAddedFriendInTheList();

        profile.clickDisconnectButton();
        profile.assertConnectButtonIsVisible();
    }

}

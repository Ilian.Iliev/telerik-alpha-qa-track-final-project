package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class EditPostPage extends BasePage {
    public EditPostPage(UserActions actions) {
        super(actions, "weare.editPostPage");
    }

    @Override
    public void assertPageNavigated() {
        super.assertPageNavigated();
        actions.waitForElementVisible("weare.editPostPage.savePostButton");
    }

    public void editPost(String isPrivate, String content, String imgPath){
        actions.selectValueFromDropdown(isPrivate.trim().toLowerCase().equals("private") ? "Private post" : "Public post",
                "weare.editPostPage.postVisibilityDropdown");
        actions.typeValueInField(content==null ? "" : content,"weare.editPostPage.contentTextField");
        if (imgPath!=null && !imgPath.trim().isEmpty())
            actions.uploadPhoto("weare.editPostPage.imageFileInput",imgPath);

        actions.clickElement("weare.editPostPage.savePostButton");
    }

    public void assertPostIsNotEdited(){
        assertPageNavigated();
        actions.waitForElementVisible("weare.editPostPage.ErrorMessage");
    }

}

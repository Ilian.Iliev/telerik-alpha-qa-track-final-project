package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class RequestsPage extends BasePage {

    public RequestsPage(UserActions actions) {
        super(actions, "weare.requestsPage");
    }

    public void clickApproveButton(){
        actions.clickElement("weare.requestsPage.approveButton");
    }
}

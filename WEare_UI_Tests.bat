@cd WebDriverBDDFramework

@mvn clean surefire-report:report -Dtest=* -Dbrowser=chrome
@mvn site -DgenerateReports=false

@cd ..
@echo Report is generated in temp\WebDriverBDDFramework\target\site

PAUSE

package tests;

import API.weare.WEAreAPI;
import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Cookie;
import pages.weare.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BaseTest {

	protected UserActions actions = new UserActions();
	protected final HomePage home = new HomePage(actions);
	protected final LoginPage login = new LoginPage(actions);
	protected final ProfilePage profile =new ProfilePage(actions);
	protected final ProfileEditorPage edit = new ProfileEditorPage(actions);
	protected final RequestsPage requests = new RequestsPage(actions);
	protected final CreatePostPage createPostPage = new CreatePostPage(actions);
	protected final PostsPage postsPage = new PostsPage(actions);
	protected final ExplorePostPage explorePostPage = new ExplorePostPage(actions);
	protected final EditPostPage editPostPage = new EditPostPage(actions);
	protected final DeletePostConfirmationPage deletePostConfirmationPage = new DeletePostConfirmationPage(actions);
	protected final RegistrationFormPage registrationFormPage = new RegistrationFormPage(actions);

	protected WEAreAPI weAreAPI = new WEAreAPI();

	@BeforeAll
	public void setUp(){
  		actions.loadBrowser("base.url");
	}

	@AfterAll
	public  void tearDown(){
		UserActions.quitDriver();
	}

	public void singIn() {
		Utils.getWebDriver().manage().addCookie(new Cookie("JSESSIONID",weAreAPI.authenticate(null)));
		home.navigateToPage();
	}

	public void logOut() {
		weAreAPI.logOut();
	}
}

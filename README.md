# WEare Social Network - Final Project, Telerik Academy Alpha QA Track

![](https://raw.githubusercontent.com/Telerik-final-project/Project-Company-Life/master/specification/assets/telerik-logo.png)

## Authors
* **Ilian Iliev** - [gitlab](https://gitlab.com/Ilian.Iliev/telerik-academy-practice)
* **Stanislav Nikolov** - [gitlab](https://gitlab.com/St-Nikolov/telerik)


WEare Social Network! The free and fair way to get what you want. Match what you have to offer with what you would like to get. Supporting your local community can bring a whole host of benefits to you and the people around you. Lending a helping hand can help you to make new friends, learn new skills, advance your career and, most importantly, give something back.


## Built With
* [Postman](https://www.getpostman.com/) - Used for API Testing
* [Newman](https://www.npmjs.com/package/newman) - Command-line collection runner for Postman
* [JBehave](hhttps://jbehave.org/) - BDD Framework used for UI Automation Testing
* [Maven](https://maven.apache.org/) - Dependency Management
* [JUnit 5.8.0](https://junit.org/junit5/docs/5.8.0-M1/user-guide/index.html) - Unit Testing Framework
* [JMeter](https://jmeter.apache.org/) - Application used to test performance
* [JMeter Plugins Manager](https://jmeter-plugins.org/wiki/PluginsManager/?fbclid=IwAR14aa_QPIHdJ9F4DAtw3i1bF1mSg_JHZEecTyiXe-udX2uVrVOceTQQACM#JMeter-Plugins-Manager) - Custom Thread Groups Plugin

## Resources
1. [Test Plan](https://drive.google.com/file/d/14KcuTC85yXhFqfnd2uphnG-Bpz08Hxse/view?usp=sharing)
2. [Test Cases](https://docs.google.com/spreadsheets/d/1okS1XI5KPlBo2OMRdmu_s-xUoJHGCu61/edit?usp=sharing&ouid=116165800053038429594&rtpof=true&sd=true)
3. [WEare Social Network Repository](https://gitlab.com/TelerikAcademy/alpha-28-qa/-/tree/master/05.%20Final%20Project/WEare%20Deploy%20ready)
4. [Logged issues due testing](https://gitlab.com/Ilian.Iliev/telerik-alpha-qa-track-final-project/-/issues)
5. [Exploratory Testing - Report](https://drive.google.com/file/d/1zvJw6eh_GXZTYULAbeP69h5MFfyW7bNK/view?usp=sharing)
6. [Performance Testing - Report](https://drive.google.com/file/d/11nxFCSiEmy6D9GUYrn0ekIHfYeCXwYTq/view?usp=sharing)
6. [Final Test Report](https://docs.google.com/presentation/d/1PRb5Z1Av389lzrbUIa7oGigSrmYwxuNd/edit?usp=sharing&ouid=116165800053038429594&rtpof=true&sd=true)

## Tests Execution
- To execute integration tests the following packages have to be installed

1.   [Newman](https://www.npmjs.com/package/newman)
2.   [HTML reporter](https://www.npmjs.com/package/newman-reporter-htmlextra)

- To execute the Jbehave tests Maven has to be installed
1.   [Maven](https://maven.apache.org/)

